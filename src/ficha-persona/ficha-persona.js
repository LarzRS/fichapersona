import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona';

class FichaPersona extends LitElement {
    static get styles() {
        return css`div {
            border: 1px solid;
            border-radius: 10px;
            padding: 10px;
            margin: 10px;
        }`
    }

    static get properties() {
        return { 
            nombre: {type: String},
            apellidos: {type: String},
            antiguedad: {type: Number},
            nivel: {type: String},
            foto: {type: Object},
            bg: {type: String}
        };
    }

    constructor() {
        super();
        this.nombre = "Pancho";
        this.apellidos = "Pantera";
        this.antiguedad = 33;
        this.foto = {};
        this.bg = "aliceblue";
    }

    render() {
        return html`
            <div style="width:400px; background-color: ${this.bg}">
                <label for="inombre" >Nombre</label>
                <input type="text" id="inombre" name="fname" value="${this.nombre}" @input="${this.updateNombre}" />
                <br>
                <label for="iapellidos" >Apellidos</label>
                <input type="text" id="iapellidos" name="fapellidos" value="${this.apellidos}" />
                <br/>
                <label for="iantiguedad" >Antiguedad</label>
                <input type="text" id="iantiguedad" name="fantiguedad" value="${this.antiguedad}" @input="${this.updateAntiguedad}" />
                <br/>
                <label for="inivel" >Nivel</label>
                <input type="text" id="inivel" name="fnivel" value="${this.nivel}" disabled/>
                <br/>
                <origen-persona @origen-set="${this.origenChange}"></origen-persona>
            </div>
        `
    }

    updated(changedProperties) {
        if(changedProperties.has("nombre")) {
        }
        if(changedProperties.has("antiguedad")) {
            this.actualizarNivel();
        }
    }

    updateNombre(e) {
        this.nombre = e.target.value;
    }

    updateAntiguedad(e) {
        this.antiguedad = e.target.value;
    }

    actualizarNivel() {
        if(this.antiguedad >= 7) {
            this.nivel = "Lider";
        } else if(this.antiguedad >=5) {
            this.nivel = "Senior";
        } else if(this.antiguedad >= 3) {
            this.nivel = "Team";
        } else {
            this.nivel = "Junior";
        }
    }

    origenChange(e) {
        var origen = e.detail.message;
        if (origen == "USA") {
            this.bg = "pink";
        } else if (origen === "México"){
            this.bg = "lightGreen";
        } else if (origen === "Canadá") {
            this.bg = "lightYellow";
        }
    }
}
customElements.define('ficha-persona', FichaPersona);